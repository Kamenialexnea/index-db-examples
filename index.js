if (!window.indexedDB) {
    window.alert("Votre navigateur ne supporte pas une version stable d'IndexedDB."
        + " Quelques fonctionnalités ne seront pas disponibles.")
}

/**
 * Dans cet exemple nous utilisons l'objet suivant
 */
const customerData = [
    { ssn: "444-44-4444", name: "Bill", age: 35, email: "bill@company.com" },
    { ssn: "555-55-5555", name: "Donna", age: 32, email: "donna@home.org" }
];

const customerData2 = [
    { ssn: "111-11-1111", name: "Leuna", age: 35, email: "leuna@company.com" },
    { ssn: "222-22-2222", name: "Steve", age: 32, email: "steve@home.org" }
]

// Ouvrons notre première base
/**
 * Si la base de données n'existe pas elle est créée.
 * 3 Représente la version
 * retourne un objet IDBOpenDBRequest
 */
var request = indexedDB.open("TestDataBase", 3)

request.onerror = function (event) {
    alert("Veuillez accepter indexed db pour une meilleure expérience");
};

request.onupgradeneeded = function (event) {
    var db = event.target.result;

    // Créer un objet de stockage qui contient les informations de nos clients. 
    // Nous allons utiliser "ssn" en tant que clé parce qu'il est garanti d'être 
    // unique - du moins, c'est ce qu'on en disait au lancement.
    var objectStore = db.createObjectStore("customers", { keyPath: "ssn" });

    // Créer un index pour rechercher les clients par leur nom. Nous pourrions 
    // avoir des doublons (homonymes), alors on n'utilise pas d'index unique.
    objectStore.createIndex("name", "name", { unique: false });

    // Créer un index pour rechercher les clients par leur adresse courriel. Nous voulons nous
    // assurer que deux clients n'auront pas la même, donc nous utilisons un index unique.
    objectStore.createIndex("email", "email", { unique: true });

    // Utiliser la transaction "oncomplete" pour être sûr que la création de l'objet de stockage
    // est terminée avant d'ajouter des données dedans.
    objectStore.transaction.oncomplete = function (event) {
        // Stocker les valeurs dans le nouvel objet de stockage.
        var customerObjectStore = db.transaction("customers", "readwrite").objectStore("customers");
        for (var i in customerData) {
            customerObjectStore.add(customerData[i]);
        }
    }

    /**
     * Exemple avec autoIncrement
     */
    var objectStore = db.createObjectStore("names", { autoIncrement: true });

    objectStore.transaction.oncomplete = function (event) {
        // Stocker les valeurs dans le nouvel objet de stockage.
        // readwrite : lecture et ecriture possible
        var customerObjectStore = db.transaction("names", "readwrite").objectStore("names");
        for (var i in customerData) {
            customerObjectStore.add(customerData[i].name);
        }
    }

    /**
     * Ajouter des données à la base de données
     * Le premier paramètre représente le nom de la table
     */
    var transaction = db.transaction(["customers"], "readwrite")

    // Faire quelque chose lorsque toutes les données sont ajoutées à la base de données.
    transaction.oncomplete = function (event) {
        alert("Toutes vos données sont sauvegardées");
    };

    transaction.onerror = function (event) {
        // N'oubliez pas de gérer les erreurs !
        console.log(event.target.result)
    };

    var objectStore = transaction.objectStore("customers")
    for (var i in customerData2) {
        var request = objectStore.add(customerData2[i]);
        request.onsuccess = function (event) {
            // event.target.result == customerData[i].ssn;
        };
    }

    /**
     * Suppression des données
     */
    var request = db.transaction(["customers"], "readwrite")
        .objectStore("customers")
        .delete("444-44-4444");
    request.onsuccess = function (event) {
        // c'est supprimé !
    };

    /**
     * Recuperer les donnees de la bd
     */
    var request = db.transaction("customers").objectStore("customers").get("444-44-4444")
    request.onsuccess = function (event) {
        alert("Name for SSN 444-44-4444 is " + event.target.result.name);
    };
    request.onerror = function (event) {
        // Erreur
    }

    /**
     * Update les informations de la base de données
     */
    var objectStore = db.transaction(["customers"], "readwrite").objectStore("customers");
    var request = objectStore.get("444-44-4444")
    request.onerror = function (event) {
        // Gestion des erreurs!
    };
    request.onsuccess = function (event) {
        // On récupère l'ancienne valeur que nous souhaitons mettre à jour
        var data = request.result;

        // On met à jour ce(s) valeur(s) dans l'objet
        data.age = 42;

        // Et on remet cet objet à jour dans la base
        var requestUpdate = objectStore.put(data);
        requestUpdate.onerror = function (event) {
            // Faire quelque chose avec l’erreur
        };
        requestUpdate.onsuccess = function (event) {
            // Succès - la donnée est mise à jour !
            alert("Update for SSN 444-44-4444 is " + JSON.stringify(data));
        };
    };

    /**
     * Lire tous les objets de la table customer en utilisant les curseurs
     */
    var customers = [];
    objectStore.openCursor().onsuccess = function (event) {
        var cursor = event.target.result;
        if (cursor) {
            customers.push(cursor.value);
            cursor.continue();
        }
        else {
            alert("Got to all customers: " + customers);
        }
    };

    /**
     * Utiliser les index pour faire les recherches
     */
    var index = objectStore.index("name");
    index.get("Donna").onsuccess = function (event) {
        alert("Donna's SSN is " + event.target.result.ssn);
        // On peut utiliser les index a ce niveau pour parcourir tous les resultats possibles
        // Ceci en utilisant openCursor()
    };
};
